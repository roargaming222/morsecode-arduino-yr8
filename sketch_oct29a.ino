bool completed = false;
int pin = 13;

void dot(int times) {
   if (times < 0){times = 1;}
   for (int i = 0; i <= times; i++) {
     digitalWrite(pin,HIGH); 
     delay (300); 
     digitalWrite(pin,LOW); 
     delay (300);
   }
}

void dash(int times) {
  if (times < 0) { times = 1; }
  for(int i = 0; i <= times; i++) {
    digitalWrite(pin, HIGH);
    delay(900);
    digitalWrite(pin, LOW);
    delay(300);  
  }  
}

void shortspace() {delay(600);}

void setup() {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
}   

void loop() {
  if (!completed) {

    /* dot(1); -> .
     * dash(1); -> -
    */
    
    // SOS
    
    dot(3);
    shortspace();   
    dash(3);
    shortspace();
    dot(3);
    shortspace();
    
    //SPACE
    shortspace();
    
    // SEND
  
    dot(3);  
    shortspace();
    dot(1);
    shortspace();
    dash(1);   
    dot(1);
    shortspace();
    dash(1);
    dot(2);
    shortspace();
    
    // HELP
 
    dot(4);
    shortspace();
    dot(1);
    shortspace();
    dot(1);
    dash(1);
    dot(2);
    shortspace();
    dot(1);
    dash(2);
    dot(1);
    
    completed = true;
  }

}